use async_lock::RwLock;
use futures::stream::SplitStream;
use futures_channel::mpsc::{unbounded, UnboundedSender};
use futures_util::{future, pin_mut, stream::TryStreamExt, StreamExt};
use std::collections::HashMap;
use std::env;
use std::sync::Arc;
use tokio::net::{TcpListener, TcpStream};
use tokio_tungstenite::tungstenite::Message;
use tokio_tungstenite::{accept_async, WebSocketStream};
use tracing::{info, instrument};

#[derive(Debug)]
struct Pawn {
    pub sender: UnboundedSender<Message>,
}

async fn broadcast(
    from_pawn: SplitStream<WebSocketStream<TcpStream>>,
    pawns: Arc<RwLock<HashMap<i32, Pawn>>>,
    pawn_id: i32,
) {
    let pawns_ref = &pawns;
    let _ = from_pawn
        .try_for_each_concurrent(None, {
            |msg| async move {
                match msg {
                    Message::Text(ref msg_text) => {
                        info!("'{}'", msg_text);

                        {
                            let local_pawns = pawns_ref.clone();
                            let lock = local_pawns.read().await;
                            let broadcast_recipients = lock
                                .iter()
                                .filter(|(other_pawn_id, _)| other_pawn_id != &&pawn_id);

                            for (other_pawn_id, other_pawn) in broadcast_recipients {
                                info!("Pawn {}: Received '{}'", other_pawn_id, msg_text);
                                // Pass along the message through the channels
                                // to the other pawns
                                let _ = other_pawn.sender.unbounded_send(msg.clone());
                            }
                        }
                    }
                    _ => (),
                }

                Ok(())
            }
        })
        .await;
}

#[instrument(skip(pawns))]
async fn pawn_control(pawn_id: i32, raw_stream: TcpStream, pawns: Arc<RwLock<HashMap<i32, Pawn>>>) {
    let ws_stream = accept_async(raw_stream)
        .await
        .expect("Error during handshake");

    // Split websocket into send and receive streams
    let (to_pawn, from_pawn) = ws_stream.split();

    // Create channels so other clients can talk to this one
    let (to_channel, from_channel) = unbounded();

    {
        let mut lock = pawns.write().await;
        lock.insert(pawn_id, Pawn { sender: to_channel });
    }

    // A future for the action of broadcasting all messages
    // from the pawn to all other pawns
    let broadcast_incoming = broadcast(from_pawn, pawns, pawn_id);

    // A future for mapping all incoming messages from
    // the channel to this pawn's outgoing websocket
    // connection.
    let receive_from_others = from_channel.map(Ok).forward(to_pawn);

    // Pin the futures for these actions to ensure that
    // switch which thread they run on doesn't invalidate
    // any self references they have
    pin_mut!(broadcast_incoming, receive_from_others);

    // Interleave the two actions, while one is idle the
    // other can work, but not both at the same time
    future::select(broadcast_incoming, receive_from_others).await;
}

fn next_id(gen: &mut i32) -> i32 {
    *gen = *gen + 1;
    return *gen;
}

#[tokio::main]
async fn main() {
    let args: Vec<String> = env::args().collect();

    let port = &args[1];
    tracing_subscriber::fmt().pretty().init();

    info!("Binding to port {}", port);

    // Bind to the port async. and wait for result
    match TcpListener::bind(format!("127.0.0.1:{}", port)).await {
        Ok(server) => {
            let pawns = Arc::new(RwLock::new(HashMap::new()));
            let mut id_gen: i32 = 0;

            // Wait for the next connection
            while let Ok((stream, _)) = server.accept().await {
                let id = next_id(&mut id_gen);
                tokio::spawn(pawn_control(id, stream, pawns.clone()));
            }
        }
        Err(_) => (),
    }
}
