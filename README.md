# Server

Manages the pawns and routes commands from front-ends

# Resources

Articles and documentation helpful for understanding the project:
- [Async Programming in Rust](https://rust-lang.github.io/async-book/01_getting_started/01_chapter.html)
- [futures_channel docs](https://docs.rs/futures-channel/0.3.13/futures_channel/)
- [futures_util docs](https://docs.rs/futures-util/0.3.13/futures_util/index.html)
- [tokio_tungstenite docs](https://docs.rs/tokio-tungstenite/0.14.0/tokio_tungstenite/index.html)
